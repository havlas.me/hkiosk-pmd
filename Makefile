.PHONY: install
install:
	install -d $(DESTDIR)/etc/default
	install -m 644 default/hkiosk-pmstated $(DESTDIR)/etc/default/hkiosk-pmstated
	install -d $(DESTDIR)/usr/lib/systemd/system
	install -m 644 systemd/hkiosk-pmstate.service $(DESTDIR)/usr/lib/systemd/system/hkiosk-pmstate.service
	install -d $(DESTDIR)/usr/lib/systemd/system/hkiosk-pmstate.service.wants
	install -m 644 systemd/hkiosk-pmstated.service $(DESTDIR)/usr/lib/systemd/system/hkiosk-pmstated.service
	install -m 644 systemd/hkiosk-pmstated.path $(DESTDIR)/usr/lib/systemd/system/hkiosk-pmstated.path
	install -d $(DESTDIR)/usr/libexec
	install -m 755 libexec/hkiosk-pmstated $(DESTDIR)/usr/libexec/hkiosk-pmstated
	install -d $(DESTDIR)/var/lib/hkiosk

.PHONY: uninstall
uninstall:
	-rm $(DESTDIR)/etc/default/hkiosk-pmstated
	-rm $(DESTDIR)/usr/lib/systemd/system/hkiosk-pmstate.service
	-rmdir $(DESTDIR)/usr/lib/systemd/system/hkiosk-pmstate.service.wants
	-rm $(DESTDIR)/usr/lib/systemd/system/hkiosk-pmstated.service
	-rm $(DESTDIR)/usr/lib/systemd/system/hkiosk-pmstated.path
	-rm $(DESTDIR)/usr/libexec/hkiosk-pmstated
	-rm $(DESTDIR)/var/lib/hkiosk/pmstate
	-rmdir $(DESTDIR)/var/lib/hkiosk
