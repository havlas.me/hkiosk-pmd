hKiosk PowerManagement Daemon
=============================

[![MIT license][license-image]][license-link]

A small daemon that watches `/var/lib/hkiosk/pmstate` file for changes and starts or stops the `hkiosk-pmstate.service`
service accordingly. The service is started when the file contains `on` and stopped when it is empty, any other value
can cause unexpected behavior.

Optionally, any service can be bound to the `hkiosk-pmstate.service` service by creating a symlink in the 
`/etc/systemd/system/hkiosk-pmstate.service.wants` directory. To ensure the service follows the `hkiosk-pmstate.service`
state it must have `PartOf=hkiosk-pmstate.service` directive in the `[Unit]` section.

Installation
------------

```shell
DESTDIR= make install
```

License
-------

[MIT][license-link]


[license-image]: https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square
[license-link]: LICENSE
