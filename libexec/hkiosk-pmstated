#!/bin/sh
# SPDX-License-Identifier: MIT
# Copyright (C) 2024, Tomáš Havlas <tomas@havlas.me>
# Distributed under the MIT License.
#
# This file is part of the hkiosk-pmstated project.
# https://gitlab.com/havlas.me/hkiosk-pmstated

set -e -u -o pipefail

if [ $# -ne 0 ]; then
    cat >&2 <<EOHELP
Usage: $( basename "$0" )
EOHELP
    exit 2
fi

PMSTATE="${PMSTATE:-/var/lib/hkiosk/pmstate}"
UNITNAME="${UNITNAME:-hkiosk-pmstate.service}"

if grep -q 'on' "$PMSTATE"; then
    systemctl start "$UNITNAME"
else
    systemctl stop "$UNITNAME"
fi
